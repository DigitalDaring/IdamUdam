import AuthCache from '../cache/index';

class UserMiddleware {

    constructor(){

    }

    requireAuthenticated(req, res, next){
        const authToken = req.headers.authorization;
        if (authToken) {
            const foundUser = AuthCache.confirmKey(authToken.toString());
            if (foundUser !== null) {
                req.user = foundUser;
                next();
            }
        } else {
            res.status(401).send();
        }
    }

}

export default new UserMiddleware();
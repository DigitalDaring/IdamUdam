import {UserModel} from "../models/user";
import AuthCache from "../cache";
import express from 'express';
import UserService from '../services/user-service';

export class AuthRequestRoutes {

    private userService = UserService;

    constructor() {

    }

    registerRoutes(router: express.Router): void {
        router.post("/auth/login", async (req, res) => {

            try {
                const foundUser = await this.userService.login(req.body.email, req.body.password)
                const key = await AuthCache.registerNewKey(foundUser);
                res.status(200).json({
                    user: foundUser,
                    key
                });
            } catch (e) {
                res.status(401).send();
            }

        });

        router.get("/auth/confirm/:id", async(req, res) => {
           try{
               const result = await this.userService.validateUser(req.params.id);
               res.status(200).json({message: 'Successfully Validated'});
           } catch (e) {
               res.status(500).json(e);
           }

        });
    }

}
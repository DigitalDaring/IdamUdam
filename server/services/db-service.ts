import * as mongodb from "mongodb";

class DbService {

    db : mongodb.Db;

    constructor(){

    }

    async connectToDB(uri: string) {
        const promise = new Promise(async (resolve, reject) => {

            try{
                const client = await mongodb.MongoClient.connect(uri);
                this.db = client.db();
                resolve();
            } catch (error) {
                reject(error);
            }


        });

        return promise;
    }

}

export default new DbService();

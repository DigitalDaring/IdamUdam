import * as bcrypt from 'bcrypt';

class EncryptionService {

    constructor() {}

    async encryptPassword(password) {

        const promise = new Promise((resolve, reject) => {
            bcrypt.genSalt(10, (err, salt) => {
                if(err){
                    reject(err);
                }

                bcrypt.hash(password, salt, (error, hash) => {

                    if(error){
                        reject(error);
                    }

                    resolve(hash);

                })
            })
        });

        return promise;

    }

    async comparePassword(plainPass, hashed) {
        const promise = new Promise((resolve, reject) => {
            bcrypt.compare(plainPass, hashed, (err, match) => {
                if(err){
                    reject(err);
                } else {
                    resolve(match);
                }
            })
        });

        return promise;
    }

}

export default new EncryptionService();
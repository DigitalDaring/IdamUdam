import {UserModel} from "../models/user";
import EncryptionService from './encryption-service';
import DbService from './db-service';
import {v4 as uuid} from 'uuid';
import {MongoClient, Db} from "mongodb";
import {ObjectID} from "bson";
import EmailService from './email-service';

class UserService {

    constructor() {

    }

    async login(email: string, password: string): Promise<UserModel> {
        const promise = new Promise<UserModel>(async(resolve, reject) => {
            try{
                const foundUser = await DbService.db.collection("users").findOne({
                    email
                });

                if(foundUser){
                    const isValidPassword =
                        await EncryptionService.comparePassword(password, foundUser.safeword);

                    if(isValidPassword) {
                        foundUser.safeword = 'banana-hammock';
                        resolve(foundUser);
                    } else {
                        reject({message: "Invalid Username or Password"});
                    }
                } else {
                    reject({message: "Invalid Username or Password"});
                }
            } catch(error) {
                reject(error);
            }
        });

        return promise;
    }


    async signup(email: string, password: string): Promise<ObjectID> {
        const promise = new Promise<ObjectID>(async (resolve, reject) => {
            try {

                const existing = await DbService.db.collection("users").findOne({
                    email
                });

                if(existing){
                    reject({message: "User already exists"})
                }

                const hashedPassword = await EncryptionService.encryptPassword(password);

                const toCreate = {
                    email,
                    safeword: hashedPassword,
                    signUpDate: new Date(),
                    confirmedDate: null,
                    confirmationCode: uuid()
                } as UserModel;

                const inserted = await DbService.db.collection("users").insertOne(toCreate);
                const result = await DbService.db.collection("users").findOne({
                    email
                });
                const sentEmail = await EmailService.sendValidationEmail(result);
                resolve(inserted.insertedId);

            } catch (error) {
                reject(error);
            }
        });

        return promise;
    }

    async validateUser(confirmationCode: string) {
        const promise = new Promise(async (resolve, reject) => {
            try {
                const relatedUser = await DbService.db.collection("users").findOne({
                    confirmationCode
                });

                if(!relatedUser){
                    reject({message: "unable to find user matching this confirmation code"});
                }

                if(relatedUser.confirmedDate !== null){
                    reject({message: "User has already confirmed sign-up!"});
                }

                const update = await DbService.db.collection("users").updateOne({
                    confirmationCode
                }, {
                    "$set": {
                        "confirmedDate": new Date()
                    }
                });

                console.log(`updated ${update} records!`);

                resolve();
            } catch (error) {
                reject(error);
            }
        });

        return promise;
    }
}

export default new UserService();
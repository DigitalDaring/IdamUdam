import {UserModel} from "../models/user";
import * as crypto from 'crypto';

class AuthCache {
    private keyMap;

    constructor(){
        this.keyMap = {};
    }

    async registerNewKey(user: UserModel): Promise<string>{
        const generatedKey = await this.createRandomKey();
        this.keyMap[generatedKey] = user;
        return Promise.resolve(generatedKey);
    }

    confirmKey(key: string): UserModel{
        const found = this.keyMap[key];
        return found;
    }

    async createRandomKey(): Promise<string> {
        const promise = new Promise<string>((resolve, reject) => {
            crypto.randomBytes(128, (err, buffer) => {
                const token = buffer.toString('hex');
                resolve(token);
            });
        });

        return promise;
    }
}

export default new AuthCache();
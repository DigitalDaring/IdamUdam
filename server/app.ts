import * as path from "path";
import express from "express";
import * as bodyParser from "body-parser";
import AuthCache from "./cache/index";
import {UserModel} from "./models/user";
import {AuthRequestRoutes} from "./routes/auth-request";
import {UsersRoutes} from "./routes/users";
import * as mongodb from "mongodb";

class App {

    express: express.Application;

    private enableCors(): void {
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            next();
        });
    }

    private middleware(): void {
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({extended: false}));
    }

    private routes(): void {
        const router = express.Router();

        router.get("/", (req, res, next) => {
            res.json({
                message: 'Hello World'
            });
        });

        router.get("/validated", (req, res, next) => {
            const authToken = req.headers.Authorization;
            if (authToken) {
                const foundUser = AuthCache.confirmKey(authToken.toString());
                if (foundUser !== null) {
                    res.status(200).json({
                        "message": `Welcome, ${foundUser.email}!`
                    });
                }
            }
            res.status(401).send();

        });

        const authRoutes = new AuthRequestRoutes();
        authRoutes.registerRoutes(router);

        const userRoutes = new UsersRoutes();
        userRoutes.registerRoutes(router);

        this.express.use('/', router);
    }

    constructor() {
        this.express = express();
        this.enableCors();
        this.middleware();
        this.routes();
    }
}

export default new App().express;
export class UserModel {
    email: string;
    safeword: string;
    signUpDate: Date;
    confirmedDate: Date;
    confirmationCode: string;
}